#!/bin/bash

# 
# Description:  Deploys required software and joins an established
#               Docker swarm cluster.
#

USERNAME=stackadmin
SWARMTOKEN="$1"
SWARMMGRIP="$2"

# Updates, installs required packages, installs better shell environment and docker if required
# Must run as root

if [[ "$EUID" -ne 0 ]]; then
	printf "Must run this as root"
	exit 1
fi

colorize() {
  printf "\033[0;32m$1\033[0m"
}

colorize "Updating & installing required packages...\n"
for i in update {,full-}upgrade auto{remove,clean}; do apt-get $i -y; done
apt-get update && apt-get upgrade -y &
apt-get install sudo cron git fish net-tools inotify-tools iputils-ping nala curl wget cockpit unzip fontconfig ufw tmux -y &
wait

# Install font
wget https://github.com/ryanoasis/nerd-fonts/releases/download/v3.1.1/Hack.zip \
	&& unzip Hack.zip -d ~/.fonts \
	&& fc-cache -fv \
	&& rm -rf Hack*

# Install starship command line
curl -O https://starship.rs/install.sh \
	&& sh ./install.sh -f \
	&& rm -f install.sh

# Install neovim
curl -LO https://github.com/neovim/neovim/releases/latest/download/nvim.appimage
chmod +x nvim.appimage
mv nvim.appimage /usr/local/bin/nvim

# Install docker
colorize "Installing Docker w/ experimental monitoring...\n"
curl -fsSL https://get.docker.com -o get-docker.sh
sh ./get-docker.sh &
wait 
touch /etc/docker/daemon.json
cat <<EOF > /etc/docker/daemon.json
{
  "metrics-addr": "0.0.0.0:9323",
  "experimental": true
}
EOF
systemctl restart docker.service &
wait

# Join the swarm
if [[ -z "${SWARMTOKEN}" ]]; then
  read -p "$(colorize "Enter your swarm worker join token: ")" SWARMTOKEN
fi 
if [[ -z "${SWARMMGRIP}" ]]; then 
  read -p "$(colorize "Enter your swarm worker IP and port number: ")" SWARMMGRIP
fi 
docker swarm join --token "$SWARMTOKEN" "$SWARMMGRIP"
if [[ $? -ne 0 ]]; colorize "Docker swarm join failed.\n"; fi

# Create stackadmin user 
useradd -G docker -m -s /bin/bash $USERNAME
colorize "Create a password for the stackadmin user\n"
passwd $USERNAME 

colorize "Done!\n"


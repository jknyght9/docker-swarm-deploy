#!/bin/bash

if [ -f ./.env ]; then
  # Setting up manager
  export NODE_ID=$(docker info -f '{{.Swarm.NodeID}}')
  docker node update --label-add swarmpit.db-data=true $NODE_ID
  docker node update --label-add swarmpit.influx-data=true $NODE_ID
  
  # Creating new compose file with environment variables
  docker compose config > docker-compose-deploy.yml
  sed -i '/published:/ s/"//g' docker-compose-deploy.yml
  tail -n +2 docker-compose-deploy.yml > docker-compose-deploy.yml.tmp
  mv docker-compose-deploy.yml.tmp docker-compose-deploy.yml

  # Deploying swarmpit 
  docker stack rm swarmpit
  docker stack deploy -c docker-compose-deploy.yml swarmpit
else 
  echo "Environment file does not exist for Swarmpit, cancelling installation."
fi 

#!/bin/bash

if [ -f ./.env ]; then
  # Creating network
  docker network create --driver=overlay --attachable traefik-public
  export NODE_ID=$(docker info -f '{{.Swarm.NodeID}}')
  docker node update --label-add traefik-public.traefik-public-certificates=true $NODE_ID

  # Generating new compose file with Environment variables
  docker compose config > docker-compose-deploy.yml
  sed -i '/published:/ s/"//g' docker-compose-deploy.yml
  tail -n +2 docker-compose-deploy.yml > docker-compose-deploy.yml.tmp
  mv docker-compose-deploy.yml.tmp docker-compose-deploy.yml

  # Deploying traefik
  docker stack rm traefik
  docker stack deploy -c docker-compose-deploy.yml traefik
else
  echo "Environment file does not exist for Traefik, cancelling installation"
fi

#!/bin/bash

if [ -f ./.env ]; then
  # Updating googles cadvisor repo URL
  sed -i 's/google/gcr.io\/cadvisor/g' docker-compose.yml
  sed -i 's/google/gcr.io\/cadvisor/g' docker-compose.traefik.yml

  # Generating new compose file with environment variables
  docker compose -f docker-compose.traefik.yml config > docker-compose-deploy.yml

  # Fixing minor syntax issues for Docker stack 
  sed -i '/published:/ s/"//g' docker-compose-deploy.yml
  tail -n +2 docker-compose-deploy.yml > docker-compose-deploy.yml.tmp
  mv docker-compose-deploy.yml.tmp docker-compose-deploy.yml

  # Start swarmprom
  docker stack rm swarmprom
  docker stack deploy -c docker-compose-deploy.yml swarmprom
else 
  echo "Environment file does not exist for Swamprom, cancelling installation."
fi 

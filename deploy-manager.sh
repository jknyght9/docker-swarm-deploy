#!/bin/bash

# 
# Description:  Deploys required software sets up Docker swarm
#               manager, sets up monitoring and administration
#               containers, and sets up git repository monitoring.
#

USERNAME=stackadmin
SWARMACTIVE=false 

# Updates, installs required packages, installs better shell environment and docker if required
# Must run as root

if [[ "$EUID" -ne 0 ]]; then
	printf "Must run this as root"
	exit 1
fi

colorize() {
  printf "\033[0;32m$1\033[0m"
}

colorize "Updating & installing required packages...\n"
for i in update {,full-}upgrade auto{remove,clean}; do apt-get $i -y; done
apt-get update && apt-get upgrade -y &
apt-get install sudo cron git fish net-tools inotify-tools iputils-ping nala curl wget cockpit unzip fontconfig ufw tmux -y &
wait

# Install font
wget https://github.com/ryanoasis/nerd-fonts/releases/download/v3.1.1/Hack.zip \
	&& unzip Hack.zip -d ~/.fonts \
	&& fc-cache -fv \
	&& rm -rf Hack*

# Install starship command line
curl -O https://starship.rs/install.sh \
	&& sh ./install.sh -f \
	&& rm -f install.sh

# Install neovim
curl -LO https://github.com/neovim/neovim/releases/latest/download/nvim.appimage
chmod +x nvim.appimage
mv nvim.appimage /usr/local/bin/nvim

# Install docker
colorize "Installing Docker w/ experimental monitoring...\n"
curl -fsSL https://get.docker.com -o get-docker.sh
sh ./get-docker.sh &
wait 
touch /etc/docker/daemon.json
cat <<EOF > /etc/docker/daemon.json
{
  "metrics-addr": "0.0.0.0:9323",
  "experimental": true
}
EOF
systemctl restart docker.service &
wait

ip_addresses=($(ifconfig | grep -oE "inet (addr:)?([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)" | awk '{print $2}'))
PS3="Select an IP address for Docker swarm: "
select ip_address in "${ip_addresses[@]}"; do 
  case "ip_address" in 
    *)
      colorize "Using $ip_address\nKEEP THIS DOCKER SWARM WORKER TOKEN FOR YOUR RECORDS\n"
      docker swarm init --advertise-addr "$ip_address" --data-path-port=9789
      if [[ $? -eq 0 ]]; then SWARMACTIVE=true; fi
      ;;
  esac
  break 
done

# Create stackadmin user 
useradd -G docker -m -s /bin/bash $USERNAME
colorize "Create a password for the stackadmin user\n"
passwd $USERNAME 

if [[ "$SWARMACTIVE" == true ]]; then
  colorize "Deploying Traefik (this will take some time)...\n"
  cd traefik
  /bin/bash ./deploy.sh
  cd ..
  sleep 120  

  colorize "Deploying Swarmpit...\n"
  cd swarmpit 
  /bin/bash ./deploy.sh
  cd ..
  sleep 60 

  colorize "Deploying SwarmProm...\n"
  cd swarmprom 
  /bin/bash ./deploy.sh
  cd ..
fi

read -p "$(colorize "Do you want to send alerts to MS Teams: ")" ALERTTEAMS
if [[ "$ALERTTEAMS" == [yY] ]]; then
  read -p "$(colorize "What is your Teams webhook URL: ")" TEAMSWHURL
  sed -i "s#WEBHOOKURL=\"\"#WEBHOOKURL=$TEAMSWHURL#g" scripts/send-teams-alert.sh 
  cp scripts/send-teams-alert.sh /usr/bin/send-teams-alert.sh 
  chown stackadmin:stackadmin /usr/bin/send-teams-alert.sh
  chmod 755 /usr/bin/send-teams-alert.sh 
  sed -i "/[[:space:]]*#\/usr\/bin\/send-teams-alert.sh.*/s/^[[:space:]]*#/    /" scripts/monitor-course-stacks.sh 
fi
read -p "$(colorize "Do you want to send alerts to Slack: ")" ALERTSLACK
if [[ "$ALERTSLACK" == [yY] ]]; then 
  read -p "$(colorize "What is your Slack webhook URL: ")" SLACKWHURL
  sed -i "s#WEBHOOKURL=\"\"#WEBHOOKURL=$SLACKWHURL#g" scripts/send-slack-alert.sh 
  cp scripts/send-slack-alert.sh /usr/bin/send-slack-alert.sh 
  chown stackadmin:stackadmin /usr/bin/send-slack-alert.sh
  chmod 755 /usr/bin/send-slack-alert.sh 
  sed -i "/[[:space:]]*#\/usr\/bin\/send-slack-alert.sh.*/s/^[[:space:]]*#/    /" scripts/monitor-course-stacks.sh
  sed -i "s#SLACK_URL=#SLACK_URL=$SLACKWHURL#g" swarmprom/.env 
fi

# Setup stack repo monitoring 
read -p "$(colorize "Enter the git repository that contains your environment stacks: ")" GITREPO 
# Create stacks folder
if [[ -n "${GITREPO}" ]]; then
  colorize "Build stacks monitoring...\n"
  git clone $GITREPO --depth 1 stacks
  mv stacks /opt/
  chown -R stackadmin:stackadmin /opt/stacks
  chmod -R g+rwx /opt/stacks
  git config --global --add safe.directory /opt/stacks

  # Create repo pull service 
  cp scripts/pull-repository.sh /usr/bin/pull-repository.sh 
  chown stackadmin:stackadmin /usr/bin/pull-repository.sh 
  chmod 755 /usr/bin/pull-repository.sh 
  cp services/pull-repository.service /etc/systemd/system/pull-repository.service 
  cp services/pull-repository.timer /etc/systemd/system/pull-repository.timer

  # Setup repo monitoring service
  cp scripts/monitor-course-stacks.sh /usr/bin/monitor-course-stacks.sh 
  chown stackadmin:stackadmin /usr/bin/monitor-course-stacks.sh 
  chmod 755 /usr/bin/monitor-course-stacks.sh 
  cp services/monitor-course-stacks.service /etc/systemd/system/monitor-course-stacks.service

  # Reload systemctl 
  systemctl daemon-reload
  systemctl enable pull-repository.timer
  systemctl enable monitor-course-stacks.service 
  systemctl restart pull-repository
  systemctl restart monitor-course-stacks
else 
  colorize "Skipping stack monitoring.\n"
fi

# Setup AWS ECR auto-login
read -p "$(colorize "Would you like to setup AWS ECR automatic login? (Y/N): ")" AWS_ECR_LOGIN
if [[ "$AWS_ECR_LOGIN" == [yY] ]]; then
  # Install AWS CLI
  curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
  unzip awscliv2.zip
  sudo ./aws/install
  rm awscliv2.zip
  # Setup credentials
  colorize "Enter your AWS_ACCESS_KEY_ID: "
  read AWS_ACCESS_KEY_ID  
  colorize "Enter your AWS_SECRET_ACCESS_KEY: "
  read AWS_SECRET_ACCESS_KEY 
  colorize "Enter your AWS region: (e.g. us-east-1): "
  read AWS_REGION
  colorize "Enter your AWS ECR registry URL (without http): "
  read AWS_ECR_REGISTRY
  mkdir /home/stackadmin/.aws
  cat <<EOF > /home/stackadmin/.aws/credentials
[default]
aws_access_key_id = $AWS_ACCESS_KEY_ID
aws_secret_access_key = $AWS_SECRET_ACCESS_KEY
EOF
  cat <<EOF > /home/stackadmin/.aws/config 
[default]
region = $AWS_REGION
EOF
  colorize "Setting up AWS ECR service...\n"
  # Setup ECR services
  sed -i "s#REGISTRY=\"\"#REGISTRY=$AWS_ECR_REGISTRY#g" scripts/docker-ecr-login.sh 
  cp scripts/docker-ecr-login.sh /usr/bin/docker-ecr-login.sh
  chown labadmin:labadmin /usr/bin/docker-ecr-login.sh 
  chmod 755 /usr/bin/docker-ecr-login.sh 
  cp services/docker-ecr-login.service /etc/systemd/system/docker-ecr-login.service
  cp services/docker-ecr-login.timer /etc/systemd/system/docker-ecr-login.timer
  systemctl daemon-reload 
  systemctl enable docker-ecr-login.timer 
  systemctl restart docker-ecr-login.timer 
else 
  colorize "Skipping AWS ECR setup.\n"
fi
colorize "Done!\n"

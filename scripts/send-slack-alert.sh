#!/bin/bash

WEBHOOKURL=""

TYPE="$1"
SUMMARY="$2"
MESSAGE="$3"

DTSTAMP="$(date +"%Y-%m-%d %H:%M:%S")"
FORMATTEDMESSAGE="{\"text\": \"$DTSTAMP ::: $TYPE ::: $MESSAGE\", \"username\": \"docker-swarm-cluster\"}"

#curl -X POST -H "Content-Type: application/json" --data-urlencode "payload=$FORMATTEDMESSAGE" "$WEBHOOKURL"
curl -X POST --data-urlencode "payload=$FORMATTEDMESSAGE" "$WEBHOOKURL"

#!/bin/bash

cd /opt/stacks
REPO=$(git config --get remote.origin.url)
printf "Pulling new content from $REPO"
git pull

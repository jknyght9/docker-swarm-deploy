#!/bin/bash

WEBHOOKURL=""

TYPE="$1"
SUMMARY="$2"
MESSAGE="$3"

case $TYPE in
  INFO)
    FORMATTEDTYPE="<span style='color:green;font-weight:bold;'>INFO</span>"
    ;;
  ERROR)
    FORMATTEDTYPE="<span style='color:red;font-weight:bold;'>ERROR</span"
    ;;
  DEBUG)
    FORMATTEDTYPE="<span style='color:blue;font-weight:bold;'>DEBUG</span>"
    ;;
  *)
    FORMATTEDTYPE="<span style='font-weight:bold;'>DEBUG</span>"
    ;;

esac

FORMATTEDMESSAGE="{\"summary\": \"$SUMMARY\", \"title\": \"$SUMMARY\", \"text\": \"$FORMATTEDTYPE $MESSAGE\", \"markdown\": true, \"themeColor\": \"0076D7\"}"

curl -H "Content-Type: application/json" -d "$FORMATTEDMESSAGE" "$WEBHOOKURL"

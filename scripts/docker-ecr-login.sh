#!/bin/bash
set -eu

REGISTRY=""

if [[ -v REGISTRY ]]; then
  aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin $REGISTRY 2>&1 
  if [ $? -eq 0 ]; then
    >&2 echo "Docker login succeeded"
    >&2 echo "Running docker update swarm services registry auth"
    SERVICES=$(docker service ls --format '{{.ID}}' 2>/dev/null)
    for SERVICE in $SERVICES; do
      docker service update -d -q --with-registry-auth $SERVICE 
    done
  else 
    >&2 echo "ERROR - Docker login failed"
    exit 1 
  fi
else 
  >&2 echo "ERROR - Registry is not set."
  exit 1 
fi

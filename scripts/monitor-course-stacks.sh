#!/bin/bash 

MONITORPATH="/opt/stacks"

alert() {
    #/usr/bin/send-slack-alert.sh "$1" "$2" "$3"
    #/usr/bin/send-teams-alert.sh "$1" "$2" "$3"
}

inotifywait -m -r -e close_write "$MONITORPATH" --format '%w%f' | 
while read -r file
do
  if [[ "$file" == *.yaml || "$file" == *.yml ]]; then
    echo "New or changed yaml file $file"
    alert "INFO" "Stack update" "Git repo change detected..."
    DIRNAME=$(dirname ${file#$MONITORPATH/})
    STACKNAME="${DIRNAME/\//-}"
    echo "Deploying stack $STACKNAME"
    alert "INFO" "Stack update" "[Re]deploying $STACKNAME"
    docker compose -f "$file" pull 
    docker stack rm "$STACKNAME"
    sleep 5
    docker stack deploy -c "$file" "$STACKNAME"
    if [ $? -eq 0 ]; then
      alert "INFO" "Stack update" "$STACKNAME deployed successfully"
    else  
      alert "ERROR" "Stack update" "Error occurred deploying $STACKNAME"
    fi
  fi
done


# Docker Swarm Cluster Deploy

This project contains a set of scripts and containers used to deploy a docker swarm cluster with an administrative and monitoring capabilties. Containers include:

| Container | Purpose |
| --- | --- |
| swarmpit | cluster/stack monitoring and management |
| swarmprom | system, cluster, and container monitoring |
| traefik | reverse proxy |

This project has the ability to monitor a git-based repository for new container stacks (docker-compose.yml) to deploy across the cluster. Stacks must use the service `deploy` directive in the docker-compose.yml file for proper deployment. 

## Requirements

Requires Debian-based operating system. Ubuntu 22 has been tested.

## Getting Started

Before deploying the containers, you **MUST** generate new `.env` files for your containers; otherwise the deployment will fail. Use the `env-sample` file in each container folder to accomplish this.

```shell
cd traefik
cp env-sample .env 
vim .env 
```

### Traefik 

Traefik uses Lets Encrypt to generate TLS certificates for secured communications. This process requires ownership of a legitimate domain and the ability to edit DNS records for your domain. Review and change the following variables.

```shell 
USERNAME=admin                  # Username to access traefik 
HASHED_PASSWORD=changeme        # Run openssl passwd -apr1 PASSWORD for this 
DOMAIN=server.domain.tld        # The servername with domain 
EMAIL=me@me.com                 # Email for the TLS certificate 
AWS_ACCESS_KEY=changeme         # AWS Route53 information if required 
AWS_SECRET_ACCESS_KEY=changeme
AWS_REGION=us-east-1
AWS_HOSTED_ZONE_ID=changeme

```

### SwarmProm 

SwarmProm uses Prometheum, Alert Manager, and other containers to monitor the node resources, container environments, etc. Like Traefik, you will need to change the USERNAME, HASHED_PASSWORD, and DOMAIN. If you have access to Slack, you can configure SwarmProm to set alerts to Slack.

```shell
USERNAME=admin                  # Username to access traefik 
HASHED_PASSWORD=changeme        # Run openssl passwd -apr1 PASSWORD for this 
DOMAIN=domain.tld               # The domain ONLY!!!! 
SLACK_URL=                      # Slack configuration if required 
SLACK_CHANNEL=
SLACK_USER=
```

SwarmProm will create several containers and will use the container name as part of the FQDN. Only put the domain information under the `DOMAIN` variable in the .env file!

### SwarmPit 

Swarpit only requires the domain (server FQDN set).

### Recommendations

We recommend naming your servers using the sample domain **acme.com**.

| Container | Domain (Server FQDN) |
| --- | --- |
| Traefik | traefik.acme.com | 
| SwarmProm | acme.com |
| SwarmPit | swarmpit.acme.com |


If you are monitoring a git repo for new container stacks, you must change the `GITREPO` variable in the `deploy.sh` script. This should include the git repository URL, username, password, or anyother credentials for proper login and cloning.

With root permissions, run the `deploy.sh` script. This will install all required programs, docker, and sets up some services for monitoring a git repository for new stacks (docker-compose files).

## Docker Swarm

During the installation process, you should have recieved a Docker Swarm Worker join token command. Once you have completed the installation of the new Docker node, run this command on the new node to join it to the swarm.

## Git repository setup and monitoring 

You will need to edit the `deploy.sh` file with the URL and credentials of the git repository you wish to setup; otherwise, it will be skipped. The `deploy.sh` script sets up a cron job to pull the most current data from your git repository. This occurs every 10 minutes.

### Stack monitoring 

The git repository containing all stacks should reside in `/opt/stacks`. A service called `monitor-course-stacks.service` will monitor this directory for any changes to YAML files. If it detects a change, it will take the full path of the YAML file, strip `/opt/stacks`, and use the output as a stack name. It will then remove any existing stacks, pull the required images, and deploy the stack. This can be monitored in **swarmpit** or via the CLI.

## AWS ECR

If you are not using AWS ECR, run the command `deploy.sh no-ecr` to skip this installation.

There are some huddles to jump for using AWS ECR container registries. After running the `deploy.sh` script, login as **stackadmin** and setup your AWS credentials.

You will need to create an AWS IAM user and have the proper policy attached. The following is an example of an [insecure] policy to grant access to ECR:

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "ecr:GetDownloadUrlForLayer",
                "ecr:BatchGetImage",
                "ecr:DescribeImages",
                "ecr:GetAuthorizationToken",
                "ecr:BatchCheckLayerAvailability"
            ],
            "Resource": "*"
        }
    ]
}
```

Next, you will need to generate a secret and access API key for this user. With this information, run the following command and input your AWS_SECRET_KEY and AWS_SECRET_ACCESS_KEY.

```shell
# Configure AWS credentials
aws configure
```

The `deploy.sh` script will install a service called `docker-ecr-login.service`. This service will use these credentials to refresh your ECR login session for new stack deployments with ECR containers.

## Troubleshooting

Administrators can review logs from all required services using the following commands:

```shell
# Review docker service logs
journalctl -u docker.service 

# Review stack monitoring service 
journalctl -u monitor-course-stacks.service

# Review docker ecr login service 
journalctl -u docker-ecr-login.service 

# Use the -f flag to follow these services
```

## Legal
This project is developed and maintained freely by its authors and contributors. This project may be used without restrictions; however, we ask that enhancements to this project be added on your behalf for review and mergeing. The user takes full responsibilty for using all or part of this project; the author or contributors are not responsible for unintended results from use.
